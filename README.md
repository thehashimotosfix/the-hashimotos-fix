I have a 100% success rate with my students. That means that if you work with me, you can have 100% confidence that your Hashimoto’s disease will permanently resolved and no longer be in your body. And I’m not talking about it going into remission. I’m talking completely gone.

Address: 7535 E Hampden Ave, Suite 352, Denver, CO 80231, USA

Phone: 720-458-6469

Website: https://thehashimotosexpert.com
